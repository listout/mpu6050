# MPU6050 C driver for ESP32

Ported driver code from [here](https://github.com/psiphi75/esp-mpu9250.git).

## Pin assignment

- master for ESP32:
  - GPIO21 is assigned as the data signal of i2c master port
  - GPIO22 is assigned as the clock signal of i2c master port

## Build and Flash

Build the project and flash it to the board, then run monitor tool to view
serial output:

```
idf.py -p PORT flash monitor
```
