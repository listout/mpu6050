#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "mpu6050.h"

static const char *tag = "main";

calibration_t cal = {
	.mag_offset = { .x = 25.183594, .y = 57.519531, .z = -62.648438 },
	.mag_scale = { .x = 1.513449, .y = 1.557811, .z = 1.434039 },
	.accel_offset = { .x = 0.020900, .y = 0.014688, .z = -0.002580 },
	.accel_scale_lo = { .x = -0.992052, .y = -0.990010, .z = -1.011147 },
	.accel_scale_hi = { .x = 1.013558, .y = 1.011903, .z = 1.019645 },

	.gyro_bias_offset = { .x = 0.303956, .y = -1.049768, .z = -0.403782 }
};

/*
 * Transformation:
 *  - Rotate around Z axis 180 degrees
 *  - Rotate around X axis -90 degrees
 * @param  {object} s {x,y,z} sensor
 * @return {object}   {x,y,z} transformed
 */
static void transform_accel_gyro(vector_t *v)
{
	float x = v->x;
	float y = v->y;
	float z = v->z;

	v->x = -x;
	v->y = -z;
	v->z = -y;
}

void get_imu_vals(vector_t *va, vector_t *vg)
{
	ESP_ERROR_CHECK(get_accel_gyro(va, vg));

	transform_accel_gyro(va);
	transform_accel_gyro(vg);

	printf("Accel: %.3f %.3f %.3f\n", va->x, va->y, va->z);
	printf("Gryos: %.3f %.3f %.3f\n", vg->x, vg->y, vg->z);
}

void imu_task(void *pvParameter)
{
	vector_t va, vg;
	for (;;) {
		get_imu_vals(&va, &vg);
		vTaskDelay(100 / portTICK_PERIOD_MS);
	}
}

void app_main(void)
{
	ESP_LOGI(tag, "MPU6050 test");

	i2c_mpu6050_init(&cal);

	xTaskCreate(imu_task, "imu task", 8192, NULL, 5, NULL);
}
